package org.example.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.models.Hotel;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HotelDto {

  private Long id;

  @Schema(description = "Hotel name")
  private String name;

  @Schema(description = "The Rooms that exists in the hotel")
  private int roomsNum;


  public static HotelDto from(Hotel hotel){
    return HotelDto
            .builder()
            .id(hotel.getId())
            .name(hotel.getName())
            .roomsNum(hotel.getRoomsNum())
            .build();
  }
}
