package org.example.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.models.Cinema;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CinemaDto {

  private Long id;

  @Schema(description = "Cinema name")
  private String name;

  public static CinemaDto from(Cinema cinema){
    return CinemaDto
            .builder()
            .id(cinema.getId())
            .name(cinema.getName())
            .build();
  }
}
