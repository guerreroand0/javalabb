package org.example.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.models.City;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CityDto {
  private Long id;

  private String name;

  private List<CinemaDto> cinemas;

  public static CityDto from(City city){
    return CityDto
            .builder()
            .id(city.getId())
            .name(city.getName())
            .build();
  }
}
