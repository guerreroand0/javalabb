package org.example.services.impl;

import org.example.dto.CinemaDto;
import org.example.dto.CinemasPage;
import org.example.models.City;
import org.example.repositories.CityRepository;
import org.example.services.CinemaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CinemaServiceImp implements CinemaService {
  @Autowired
  private CityRepository cityRepository;

  @Override
  public CinemasPage getCinemasFromCity(Long cityId) {
    City city = cityRepository.findById(cityId).get();

    List<CinemaDto> cinemas = null;

    if(city != null){
      cinemas = city.getCinemas()
              .stream()
              .map(c -> CinemaDto.from(c))
              .toList();
    }
    
    return CinemasPage
            .builder()
            .cinemas(cinemas)
            .build();
  }
}
