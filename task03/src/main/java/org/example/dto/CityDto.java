package org.example.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.models.City;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CityDto {
  private Long id;

  private String name;

  private List<CinemaDto> cinemas;

  private List<HotelDto> hotels;

  public static CityDto from(City city){
    return CityDto
            .builder()
            .id(city.getId())
            .name(city.getName())
            .cinemas(city
                    .getCinemas()
                    .stream()
                    .map(c -> CinemaDto.from(c))
                    .toList())
            .hotels(city
                    .getHotels()
                    .stream()
                    .map(h -> HotelDto.from(h))
                    .toList())
            .build();
  }
}
