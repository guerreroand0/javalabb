package org.example.controllers.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.example.dto.CinemasPage;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/api/cities/{city_id}/cinemas")
public interface CinemaApi {
  @Operation(summary = "We get a list of hotels", description = "Available for all users")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "Запрос обработан успешно",
                  content = {
                          @Content(mediaType = "application/json", schema = @Schema(implementation = CinemasPage.class))
                  })
  })
  @GetMapping
  ResponseEntity<CinemasPage> getHotels(@PathVariable("city_id") Long cityId);

}
