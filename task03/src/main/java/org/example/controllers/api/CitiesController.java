package org.example.controllers.api;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.example.dto.CitiesPage;
import org.example.services.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class CitiesController implements CityApi{

  @Autowired
  private CityService cityService;

  @Override
  public ResponseEntity<CitiesPage> getCities() {
    return ResponseEntity.ok(cityService.getCities());
  }
}
