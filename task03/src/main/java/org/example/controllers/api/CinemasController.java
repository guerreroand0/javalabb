package org.example.controllers.api;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.example.dto.CinemasPage;
import org.example.services.CinemaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class CinemasController implements CinemaApi {

  @Autowired
  private CinemaService cinemaService;

  @Override
  public ResponseEntity<CinemasPage> getHotels(Long cityId) {
    return ResponseEntity.ok(cinemaService.getCinemasFromCity(cityId));
  }
}
