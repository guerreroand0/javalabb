package org.example.controllers.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

import org.example.dto.CitiesPage;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(path = "/api/cities")
public interface CityApi {
  @Operation(summary = "We get a list of hotels", description = "Available for all users")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "Запрос обработан успешно",
                  content = {
                          @Content(mediaType = "application/json", schema = @Schema(implementation = CitiesPage.class))
                  })
  })
  @GetMapping
  ResponseEntity<CitiesPage> getCities();

}
