package org.example.controllers.api;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.example.dto.HotelsPage;
import org.example.services.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class HotelsController implements HotelApi {

  @Autowired
  private HotelService hotelService;

  @Override
  public ResponseEntity<HotelsPage> getHotels(Long cityId) {
    return ResponseEntity.ok(hotelService.getHotelsFromCity(cityId));
  }
}
