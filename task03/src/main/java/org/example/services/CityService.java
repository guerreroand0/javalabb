package org.example.services;

import org.example.dto.CitiesPage;

public interface CityService {
  CitiesPage getCities();
}
