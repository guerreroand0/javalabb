package org.example.services;

import org.example.dto.HotelsPage;

public interface HotelService {
  HotelsPage getHotelsFromCity(Long cityId);
}
