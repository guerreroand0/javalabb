package org.example.services;

import org.example.dto.CinemasPage;

public interface CinemaService {
  CinemasPage getCinemasFromCity(Long cityId);
}
