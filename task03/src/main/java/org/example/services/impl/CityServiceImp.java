package org.example.services.impl;

import org.example.controllers.api.CityApi;
import org.example.dto.CitiesPage;
import org.example.dto.CityDto;
import org.example.repositories.CityRepository;
import org.example.services.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityServiceImp implements CityService {

  @Autowired
  CityRepository cityRepository;

  @Override
  public CitiesPage getCities() {

    List<CityDto> cities =  cityRepository
            .findAll()
            .stream()
            .map(city -> CityDto.from(city))
            .toList();

    return CitiesPage
            .builder()
            .cities(cities)
            .build();
  }
}
