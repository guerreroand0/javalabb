package org.example.services.impl;

import org.example.dto.HotelDto;
import org.example.dto.HotelsPage;
import org.example.models.City;
import org.example.repositories.CityRepository;
import org.example.services.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HotelServiceImpl implements HotelService {
  @Autowired
  private CityRepository cityRepository;

  @Override
  public HotelsPage getHotelsFromCity(Long cityId) {
    City city = cityRepository.findById(cityId).get();

    List<HotelDto> hotels = null;

    if(city != null){
      hotels = city.getHotels()
              .stream()
              .map(h -> HotelDto.from(h))
              .toList();
    }

    return HotelsPage
            .builder()
            .hotels(hotels)
            .build();
  }
}
